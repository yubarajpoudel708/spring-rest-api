package com.example.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//restful services
@RestController
public class NewsService {
//	@Autowired
//	private NewsRepository newsRepository;

	private String id = "2", title = "10", image = "20";

//	{"id":"1", "title":"test", "image":"http://www.gmail.com"}
	@RequestMapping(value = "/news", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getNews() {
//		String pattern = "[{\"id\":\"%s\", \"title\":\"%s\", \"image\":\"%s\"}]";
//		String json = String.format(pattern, id, title, image);
//		System.out.println(json);
//		return json;
//		try {
//			
//			URL url = new URL("http://brt.yubrajpoudel.com.np/v1/api/news");
//			HttpURLConnection con = (HttpURLConnection) url.openConnection();
//			con.setRequestMethod("GET");
//			BufferedReader in = new BufferedReader(
//					  new InputStreamReader(con.getInputStream()));
//					String inputLine;
//					StringBuffer content = new StringBuffer();
//					while ((inputLine = in.readLine()) != null) {
//					    content.append(inputLine);
//					}
//					in.close();
//					
//					return inputLine;
//					
//		}catch(IOException e) { 
//			e.printStackTrace();
//			return e.getMessage();
//		}
		Connection con = null;
		JSONArray jsonArray = new JSONArray();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/kantipur", "root", "root");
			System.out.println("Connection is null" + (con == null));
			String SQL = "select * from news limit 3";
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(SQL);
//			System.out.println("resultset size = " + rs.next());
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("id",rs.getInt("id"));
				obj.put("news_title", rs.getString("title"));
				jsonArray.put(obj);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArray.toString();
	}
	
	@RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getNewsById(
	  @PathVariable("id") long id) {
		JSONObject jsonObject =  new JSONObject();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/kantipur", "root", "root");
			System.out.println("Connection is null" + (con == null));
			String SQL = "select * from news where id = "+ id;
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(SQL);
			while (rs.next()) {
				jsonObject.put("id",rs.getInt("id"));
				jsonObject.put("news_title", rs.getString("title"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}


	/**
	 * @param id
	 * @param title
	 * @param image
	 * @return
	 */

	@RequestMapping(value = "/news", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public String updateNews(String id, String title, String image) {
		this.title = title;
		this.image = image;
		this.id = id;
		String pattern = "{\"id\":\"%s\", \"title\":\"%s\", \"image\":\"%s\"}";
		String json = String.format(pattern, id, title, image);
		System.out.println(json);
		return json;
	}

	@RequestMapping(value = "/news/id", method = RequestMethod.GET)
	public String getId() {
		return id;
	}

	@RequestMapping(value = "/news/title", method = RequestMethod.GET)
	public String getTitle() {
		return title;
	}

	@RequestMapping(value = "/news/image", method = RequestMethod.GET)
	public String getImage() {
		return image;
	}
//	@RequestMapping(value = "/web/news", method = RequestMethod.GET)
//	public @ResponseBody Iterable<News> getAllUsers() {
//		// This returns a JSON or XML with the users
//		return newsRepository.findAll();
//	}

}
